const { defineConfig } = require('cypress')

module.exports = defineConfig({
  defaultCommandTimeout: 30000,
  pageLoadTimeout: 240000,
  chromeWebSecurity: false,
  video: false,

  e2e: {
    specPattern: "cypress/e2e/**/*.js",
    env: {
      hideCredentials: true,
      requestMode: true,
    },
    experimentalRunAllSpecs: true,
  },
  fixturesFolder: false,
  video: false,
})

