import { testElements } from "../../../support/elements"

describe('Adicionar novo usuário com sucesso', () => {

beforeEach(() => {
    cy.visit('https://demo.automationtesting.in/Register.html')
})

    it('Preencher todos os dados e clicar em "Submit" ', () => {
        //Campos Full Name possui comportamentos incorretos: Não possui limite de caracteres e aceita caracteres especiais
        cy.get(testElements.typeFirstName).type("Prime")
        cy.get(testElements.typeLastName).type("Test")
        cy.get(testElements.typeAddress).type("Test Address")
        cy.get(testElements.typeEmailAddress).type("email.test@gmail.com")
        //Campo "Phone" possui formato errado para telefone, exige 10 digitos
        cy.get(testElements.typePhone).type("1234567895")
        cy.get(testElements.checkGender).click()
        cy.get(testElements.clickSkills).select("APIs")
        cy.get (testElements.checkHobbies).click()
        cy.get(testElements.clickLanguages).click()
        cy.get(testElements.selectLanguages).click()
        //Campo Country não apresenta opções para seleção
        cy.get(testElements.clickCountry).click()
        cy.get(testElements.assertCountry).click()
        cy.get(testElements.selectYear).select("1998")
        cy.get(testElements.selectMonth).select("January")
        cy.get(testElements.selectDay).select("1")
        cy.get(testElements.typePassword).type("123456")
        cy.get(testElements.typeConfirmPassword).type("123456")
        cy.get(testElements.clickSubmit).click()

        //Validação da Tela de sucesso - Cenário não é possível de ser executado pois o campo com bug impede o Submit do Formulário
        //cy.get(testElements.succesScreen).should('contain', 'success')
    })
})