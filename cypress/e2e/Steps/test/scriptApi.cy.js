/* global Given, Then, When */
import { testElements } from "../../../support/elements"

it ('Validar consumo de API', () => {
    //puxar comando personalizado de acesso à API
    cy.submitFormData('/api/submit-form', {
        acao: 'gerar_pessoa',
        pontuacao: 'N',
        sexo: 'H',
        txt_qtde: '1'
      }).then(response => {
        //1° - Validar o campo Status Code (Resultado esperado deve ser 200)
        expect(response.status).to.equal(200)
        cy.wrap(response.body)
          .each((item) => {
            //2° - Validar se o campo idade consta no ResponseBody (Resultado esperado deve ser Sucesso)
            expect(item).to.have.property('idade')
            //4° - Validar se o valor do campo sexo do ResponseBody é igual a Feminino (Resultado esperado deve ser Falha)
            expect(item.sexo).to.not.equal('Feminino')
            //3° - Validar se o valor do campo sexo do ResponseBody é igual a Masculino (Resultado esperado deve ser Sucesso)
            expect(item.sexo).to.equal('Masculino')
            //Fazer Validação do CPF no 4Devs
            //5° - Armazenar o campo CPF do ResponseBody em uma variável nomeada CPF.
            let CPF = item.cpf.replace(/["']/g, ''); // Armazena o CPF em uma variável
            cy.request({
              method: 'POST',
              url: 'https://www.4devs.com.br/ferramentas_online.php',
              form: true,
              body: {
                acao: 'validar_cpf',
                txt_cpf: CPF // Passa a variável CPF como parâmetro
              }
            }).then((response) => {
              expect(response.status).to.equal(200)
              //6° Verificar se o CPF é valido fazendo a chamada do serviço validar cpf do 4devs, passando como parâmetro no campo txt cpf a variável setada no passo 4. (Resultado Esperado Verdadeiro)
              expect(response.body).to.include('Verdadeiro')
            })
            //7° - Limpar a variável CPF.
            CPF = ''; // Limpar a variável CPF
          })
      })      
})
