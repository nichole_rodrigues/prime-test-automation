export const testElements = 
{
    //Web
    typeFirstName : ':nth-child(1) > :nth-child(2) > .form-control',
    typeLastName: ':nth-child(1) > :nth-child(3) > .form-control',
    typeAddress: '.col-md-8 > .form-control',
    typeEmailAddress: '#eid > .form-control',
    typePhone: ':nth-child(4) > .col-md-4 > .form-control',
    checkGender: ':nth-child(1) > .ng-pristine',
    checkHobbies: '#checkbox3',
    clickLanguages: '#msdd',
    clickSkills: '#Skills',
    selectLanguages: ':nth-child(36) > .ui-corner-all',
    //campo "Country" apresenta bug ao ser clicado
    clickCountry: '.select2-selection__arrow',
    selectCountry: '[data-layer="Content"]',
    assertCountry: '#select2-country-results > :nth-child(2)',
    selectYear: '#yearbox',
    selectMonth: ':nth-child(11) > :nth-child(3) > .form-control',
    selectDay: '#daybox',
    typePassword: '#firstpassword',
    typeConfirmPassword: '#secondpassword',
    clickSubmit: '#submitbtn',
    clickChooseFile: '#imagesrc',

}