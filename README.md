# README #

Test Prime Control

Automação de API e Web durante Teste da Prime Control

Para executar este projeto você precisará das seguintes ferramentas: Git Node NPM e Cypress

Após baixar o projeto executar os comandos: $~ npm install e depois $~ npm init -y

E para abrir o Cypress execute: $~ npx cypress open

Após abrir tela de Boas Vindas ao Cypress selecione a opção "E2E Testing", em seguida escolha um navegador (exemplo: Chrome) e clique em "Start E2E Testing in Crhome".
O navegador será aberto e apresentará as Specs com scripts automatizados.
    Para executar o teste de api: clicar em scriptApi.cy.js
    Para executar o teste Web: clicar em scriptWeb.cy.js
O script é rodado automaticamente 

Desenvolvido por:
Nichole Rodrigues - nichole_santos@outlook.com
